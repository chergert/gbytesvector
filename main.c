#include <glib.h>
#include "g-bytes-vector.h"

static void
test1 (void)
{
   GBytesVector *vector;

   vector = g_bytes_vector_new();
   g_bytes_vector_free(vector);
}

static void
test2 (void)
{
   GBytes *b = g_bytes_new_static("test data", 10);
   GBytes *c = g_bytes_new_static("some data", 10);
   GBytesVector *v;

   v = g_bytes_vector_new();
   g_bytes_vector_append(v, b);
   g_bytes_vector_append(v, c);
   g_assert_cmpint(20, ==, g_bytes_vector_get_size(v));
   g_bytes_unref(b);
   g_bytes_unref(c);
   g_bytes_vector_free(v);
}

static void
test3 (void)
{
   gint32 real_val = GINT32_TO_LE(1234567);
   const guint8 *buf = (guint8 *)&real_val;
   gint32 real_val2 = GINT32_TO_LE(41236789);
   const guint8 *buf2 = (guint8 *)&real_val2;
   gint32 val;
   GBytes *b = g_bytes_new_static(buf, sizeof real_val);
   GBytes *c = g_bytes_new_static(buf2, sizeof real_val2);
   GBytesVector *v;

   v = g_bytes_vector_new();
   g_bytes_vector_append(v, b);
   g_bytes_vector_append(v, c);
   g_assert_cmpint(8, ==, g_bytes_vector_get_size(v));

   g_assert(g_bytes_vector_read_int32(v, &val));
   g_assert_cmpint(val, ==, 1234567);
   g_assert_cmpint(4, ==, g_bytes_vector_get_size(v));

   g_assert(g_bytes_vector_read_int32(v, &val));
   g_assert_cmpint(val, ==, 41236789);
   g_assert_cmpint(0, ==, g_bytes_vector_get_size(v));

   g_bytes_unref(b);
   g_bytes_unref(c);
   g_bytes_vector_free(v);
}

static void
test4 (void)
{
   gint32 real_val = GINT64_TO_LE(1234567);
   const guint8 *buf = (guint8 *)&real_val;
   gint32 real_val2 = GINT32_TO_LE(41236789);
   const guint8 *buf2 = (guint8 *)&real_val2;
   gint64 val;
   GBytes *b = g_bytes_new_static(buf, sizeof real_val);
   GBytes *c = g_bytes_new_static(buf2, sizeof real_val2);
   GBytesVector *v;

   v = g_bytes_vector_new();
   g_bytes_vector_append(v, b);
   g_bytes_vector_append(v, c);
   g_assert_cmpint(8, ==, g_bytes_vector_get_size(v));

   g_assert(g_bytes_vector_read_int64(v, &val));
   g_assert_cmpint(val, ==, 177110660148287111L);

   g_bytes_unref(b);
   g_bytes_unref(c);
   g_bytes_vector_free(v);
}

gint
main (gint   argc,
      gchar *argv[])
{
   g_test_init(&argc, &argv, NULL);
   g_test_add_func("/BytesVector/new", test1);
   g_test_add_func("/BytesVector/append", test2);
   g_test_add_func("/BytesVector/read", test3);
   g_test_add_func("/BytesVector/read2", test4);
   return g_test_run();
}
