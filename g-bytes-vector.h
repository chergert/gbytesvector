/* g-bytes-vector.h
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef G_BYTES_VECTOR_H
#define G_BYTES_VECTOR_H

#include <glib.h>

G_BEGIN_DECLS

typedef struct _GBytesVector GBytesVector;

GBytesVector *g_bytes_vector_new         (void);
void          g_bytes_vector_append      (GBytesVector *vector,
                                          GBytes       *bytes);
void          g_bytes_vector_free        (GBytesVector *vector);
gsize         g_bytes_vector_get_size    (GBytesVector *vector);
gboolean      g_bytes_vector_read        (GBytesVector  *vector,
                                          guint8        *buffer,
                                          gsize          n_bytes);
gboolean      g_bytes_vector_read_int32  (GBytesVector  *vector,
                                          gint32        *value);
gboolean      g_bytes_vector_read_int64  (GBytesVector  *vector,
                                          gint64        *value);
gboolean      g_bytes_vector_read_uint32 (GBytesVector  *vector,
                                          guint32       *value);
gboolean      g_bytes_vector_read_uint64 (GBytesVector  *vector,
                                          guint64       *value);


G_END_DECLS

#endif /* G_BYTES_VECTOR_H */
