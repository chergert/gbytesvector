/* g-bytes-vector.c
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>

#include "g-bytes-vector.h"

/**
 * SECTION:g-bytes-vector
 * @title: GBytesVector
 * @short_description: A helper for reading from a group of #GBytes.
 *
 * #GBytesVector provides a way to collect a group of #GBytes and read
 * over them as if they were one buffer of bytes. You can read raw
 * buffers or read integers from the buffer. Integers are expected to
 * be in Little Endian in the wire format. If that is not the case,
 * you will want to take the result and GINT32_FROM_LE() or similar
 * on the result.
 */

struct _GBytesVector
{
   GQueue *queue;
   gsize   offset;
};

/**
 * g_bytes_vector_new:
 *
 * Creates a new #GBytesVector.
 *
 * Returns: The newly created #GBytesVector.
 */
GBytesVector *
g_bytes_vector_new (void)
{
   GBytesVector *vector;

   vector = g_slice_new(GBytesVector);
   vector->queue = g_queue_new();

   return vector;
}

/**
 * g_bytes_vector_free:
 * @vector: (in): A #GBytesVector.
 *
 * Frees the vector and all of the #GBytes associated with it.
 */
void
g_bytes_vector_free (GBytesVector *vector)
{
   GBytes *bytes;

   if (vector) {
      while ((bytes = g_queue_pop_head(vector->queue))) {
         g_bytes_unref(bytes);
      }
      g_queue_free(vector->queue);
      g_slice_free(GBytesVector, vector);
   }
}

/**
 * g_bytes_vector_get_size:
 * @vector: (in): A #GBytesVector.
 *
 * Sums the size of all of the available vectors.
 *
 * Returns: A #gsize.
 */
gsize
g_bytes_vector_get_size (GBytesVector *vector)
{
   GBytes *bytes;
   gsize size = 0;
   guint length;
   guint i;

   length = g_queue_get_length(vector->queue);

   for (i = 0; i < length; i++) {
      bytes = g_queue_peek_nth(vector->queue, i);
      size += g_bytes_get_size(bytes);
   }

   size -= vector->offset;

   return size;
}

/**
 * g_bytes_vector_append:
 * @vector: (in): A #GBytesVector.
 * @bytes: (in): A #GBytes to append to the vector.
 *
 * Appends @bytes to @vector so that they may be read using
 * g_bytes_vector_read() and similar.
 */
void
g_bytes_vector_append (GBytesVector *vector,
                       GBytes       *bytes)
{
   g_queue_push_tail(vector->queue, g_bytes_ref(bytes));
}

/**
 * g_bytes_vector_read:
 * @vector: (in): A #GBytesVector.
 * @buffer: (out) (array-length n_bytes): The buffer to read into.
 * @n_bytes: (in): The number of bytes to read into @buffer.
 *
 * Reads the number of bytes requested by @n_bytes into @buffer. If all
 * of the bytes could be read, then %TRUE is returned. Otherwise %FALSE.
 *
 * Returns: %TRUE if successful; otherwise %FALSE.
 */
gboolean
g_bytes_vector_read (GBytesVector *vector,
                     guint8       *buffer,
                     gsize         n_bytes)
{
   GBytes *bytes;
   gsize n_read = 0;
   gsize cur_read;

   g_return_val_if_fail(vector, FALSE);
   g_return_val_if_fail(buffer, FALSE);
   g_return_val_if_fail(n_bytes, FALSE);

   /*
    * Loop while we still have bytes to read.
    */
   while (n_bytes) {
      /*
       * Make sure there is a GBytes buffer to read from.
       */
      if (!(bytes = g_queue_peek_head(vector->queue))) {
         return FALSE;
      }

      /*
       * Determine how many bytes we can read from this buffer.
       * We may overlap and have to read from multiple buffers.
       */
      cur_read = MIN(n_bytes, g_bytes_get_size(bytes));

      /*
       * Copy the data from the GBytes into the target buffer.
       */
      memcpy(buffer + n_read,
             g_bytes_get_data(bytes, NULL) + vector->offset,
             cur_read);

      /*
       * Increase our counters so we know where to perform our
       * next read from.
       */
      n_read += cur_read;
      n_bytes -= cur_read;
      vector->offset += cur_read;

      /*
       * If we are at the end of the GBytes, we can pop it off
       * the queue and release its memory.
       */
      if (vector->offset == g_bytes_get_size(bytes)) {
         g_queue_pop_head(vector->queue);
         g_bytes_unref(bytes);
         vector->offset = 0;
      }
   }

   return TRUE;
}

/**
 * g_bytes_vector_read_int32:
 * @vector: (in): A #GBytesVector.
 * @value: (out): A location for a #gint32.
 *
 * Reads a #gint64 from the vector of buffers in @vector. If successful
 * then @value is set and %TRUE is returned.
 *
 * Returns: %TRUE if successful; otherwise %FALSE.
 */
gboolean
g_bytes_vector_read_int32 (GBytesVector *vector,
                           gint32       *value)
{
   gboolean ret;

   if ((ret = g_bytes_vector_read(vector, (guint8 *)value, sizeof *value))) {
      *value = GINT32_FROM_LE(*value);
   }

   return ret;
}

/**
 * g_bytes_vector_read_uint32:
 * @vector: (in): A #GBytesVector.
 * @value: (out): A location for a #guint32.
 *
 * Reads a #guint64 from the vector of buffers in @vector. If successful
 * then @value is set and %TRUE is returned.
 *
 * Returns: %TRUE if successful; otherwise %FALSE.
 */
gboolean
g_bytes_vector_read_uint32 (GBytesVector *vector,
                            guint32      *value)
{
   gboolean ret;

   if ((ret = g_bytes_vector_read(vector, (guint8 *)value, sizeof *value))) {
      *value = GUINT32_FROM_LE(*value);
   }

   return ret;
}

/**
 * g_bytes_vector_read_int64:
 * @vector: (in): A #GBytesVector.
 * @value: (out): A location for a #gint64.
 *
 * Reads a #gint64 from the vector of buffers in @vector. If successful
 * then @value is set and %TRUE is returned.
 *
 * Returns: %TRUE if successful; otherwise %FALSE.
 */
gboolean
g_bytes_vector_read_int64 (GBytesVector *vector,
                           gint64       *value)
{
   gboolean ret;

   if ((ret = g_bytes_vector_read(vector, (guint8 *)value, sizeof *value))) {
      *value = GINT64_FROM_LE(*value);
   }

   return ret;
}

/**
 * g_bytes_vector_read_uint64:
 * @vector: (in): A #GBytesVector.
 * @value: (out): A location for a #guint64.
 *
 * Reads a #guint64 from the vector of buffers in @vector. If successful
 * then @value is set and %TRUE is returned.
 *
 * Returns: %TRUE if successful; otherwise %FALSE.
 */
gboolean
g_bytes_vector_read_uint64 (GBytesVector *vector,
                            guint64      *value)
{
   gboolean ret;

   if ((ret = g_bytes_vector_read(vector, (guint8 *)value, sizeof *value))) {
      *value = GUINT64_FROM_LE(*value);
   }

   return ret;
}
